package io.swagger.api;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.galan.commons.net.flux.Flux;
import de.galan.commons.net.flux.HttpClientException;
import de.galan.commons.net.flux.Response;
import io.swagger.model.Skript;
import io.swagger.model.Skripte;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-03-25T19:18:38.938Z[GMT]")
@RestController
public class SkriptApiController implements SkriptApi {

    private static final Logger log = LoggerFactory.getLogger(SkriptApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;
    
   

    @org.springframework.beans.factory.annotation.Autowired
    public SkriptApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Skripte> skriptGet() {
        String accept = request.getHeader("Accept");
        
        System.out.println(accept);
        
        if (accept != null && accept.contains("application/json")) {
           /*TODO*/
			return null;
        }

        return new ResponseEntity<Skripte>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> skriptPost(@Parameter(in = ParameterIn.DEFAULT, description = "", required=true, schema=@Schema()) @Valid @RequestBody Skript body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> skriptnameGet() {
    	 try {
    		 	
				Response response = Flux.request("http://ip172-18-0-31-c1edgf8h550g00d5g520-8004.direct.labs.play-with-docker.com:8004/ocpu/library/stats/R/").get();
				System.out.println(response.getStreamAsString());
				response.close();
				
				 return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
			} catch (IOException | HttpClientException e) {
			    log.error("Couldn't serialize response for content type application/json", e);
			   
			}
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
