package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Skript
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-03-25T19:18:38.938Z[GMT]")


public class Skript   {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("pfad")
  private String pfad = null;

  public Skript name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
   **/
  @Schema(example = "Eric", description = "")
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Skript pfad(String pfad) {
    this.pfad = pfad;
    return this;
  }

  /**
   * Get pfad
   * @return pfad
   **/
  @Schema(example = "/lib", description = "")
  
    public String getPfad() {
    return pfad;
  }

  public void setPfad(String pfad) {
    this.pfad = pfad;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Skript skript = (Skript) o;
    return Objects.equals(this.name, skript.name) &&
        Objects.equals(this.pfad, skript.pfad);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, pfad);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Skript {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    pfad: ").append(toIndentedString(pfad)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
